import Promise from 'bluebird';
import request from 'request';
import cheerio from 'cheerio';
import dotenv from 'dotenv';

dotenv.config(); //imports all .evn variables


/**
 * Grabs source from a specific url
 * @param url url to get source from
 */
function getSource(url) {
    return new Promise((resolve, reject) => {
        request.get(url, (error, response, body) => {
            if(!error) resolve(body);
            else reject(error);
        });
    });
}

/**
 * return a promise for games with gold
 * @param source the source of the webpage
 */
function GamesWithGold (source) {
    return new Promise((resolve, reject) => {
        var $ = cheerio.load(source)("div.game").toArray();
        var games = [];
        $.map(($) => {
            var game = {};
            var $$ = cheerio.load($).html();
            //todo: use dates to get URL (will do later) use $$$ to get the link if available
            game.image = cheerio.load($$)('.boximg')[0].attribs.src;
            game.title = cheerio.load($$)('.h3')[0].children[0].data;
            game.description = cheerio.load($$)('p')[0].children[0].data;
            var dates = cheerio.load($$)('span.semibold')[0].children[0].data;
            game.startDate = new Date(new Date().getFullYear() + '-' +
                dates.split(/[–-]+/)[0].split(/\//)[0] + '-' +
                dates.split(/[–-]+/)[0].split(/\//)[1]);
            game.endDate = new Date(new Date().getFullYear() + '-' +
                dates.split(/[–-]+/)[1].split(/\//)[0] + '-' +
                dates.split(/[–-]+/)[1].split(/\//)[1]);
            if(new Date().getTime() > new Date(game.startDate).getTime() &&
                new Date(game.endDate).getTime() > new Date().getTime()) {
                var $$$ = cheerio.load(cheerio.load($$)('div.cta-signedIn')[0]).html();
                game.url = cheerio.load($$$)('a')[0].attribs.href;
            }
            games.push(game);
        });
        resolve(games);
    });
}

/**
 * return a promise for game deals with gold
 * @param source the source of the webpage
 */
function DealsWithGold (source) {
    return new Promise((resolve, reject) => {
        var $ = cheerio.load(source)("div.game").toArray();
        var games = [];
        $.map(($) => {
            var $$ = cheerio.load($).html();
            var $$$ = cheerio.load(cheerio.load($$)('div.cta-signedIn')[0]).html();
            //todo: use dates to get URL (will do later) use $$$ to get the link if available
            var game = {};
            game.image = cheerio.load($$)('.boximg')[0].attribs.src;
            game.title = cheerio.load($$)('.h3')[0].children[0].data;
            game.discountedPrice  = cheerio.load($$)('p')[0].children[0].data;
            game.regularPrice = cheerio.load($$)('p')[1].children[0].data;
            game.gameURL = cheerio.load($$$)('a')[0].attribs.href;
            games.push(game);
        });
       resolve(games);
    });
}

/**
 * will return a json doc of current games with gold
 */
export function getGamesWithGold() {
    return new Promise((resolve, reject) => {
        getSource(process.env.GAMES_WITH_GOLD_URL)
            .then((source) => {
                return GamesWithGold(source);
            })
            .then((games) => {
                resolve(games);
            })
            .catch((error) => {
                reject(error);
            })
    });
}

/**
 * will return a json doc of current game deals with gold
 */
export function getDealsWithGold() {
    return new Promise((resolve, reject) => {
        getSource(process.env.DEALS_WITH_GOLD_URL)
            .then((source) => {
                return DealsWithGold(source);
            })
            .then((games) => {
                resolve(games);
            })
            .catch((error) => {
                reject(error);
            })
    });
}